# Networking Notes

1. Connecting from TLCF to NCCS (tested on Mac)
---

Not all of this is necessary but it works.

**on localhost:**
```
ssh-add -s /usr/local/lib/opensc-pkcs11.so
```

put the following in your `.ssh/config`:
```
Host *
Protocol 2
ForwardAgent yes
ForwardX11  yes
ForwardX11Trusted   yes
PKCS11Provider  /usr/local/lib/opensc-pkcs11.so

Host otlcf04                                                                              
ControlMaster   auto
ControlPath ~/.ssh/multiplex/master-%r@%h:%p
ControlPersist 3600
LocalForward 8123 ozoneweb:8889
LocalForward 8124 lm:7070
ProxyCommand ssh modbst1.modaps.eosdis.nasa.gov nc %h %p
ServerAliveCountMax 30
ServerAliveInterval 300
TCPKeepAlive yes
```

**on TLCF:**
```
mkdir -p .ssh
```
put the following in your TLCF `.ssh/config`:
```
# .ssh/config for connecting from TLCF to discover
Host discover 
HostName discover.nccs.nasa.gov
User dhaffner
LogLevel Quiet
ProxyCommand ssh -l dhaffner login.nccs.nasa.gov direct %h
Protocol 2
ForwardAgent yes
ForwardX11  yes
ForwardX11Trusted  yes
```

then

```
ssh discover
```
