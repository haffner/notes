## VLDIORT TESTING

Outputs needed from the WLS to help interpret calculations:

1. Lup(VZA, RAA) upwelling radiance at the water surface (BOA) for a list of user specified angles.
2. Lw(VZA, RAA) water leaving radiance for a list of user angles.
3. Fresnel reflectance(VZA, RAA) at BOA for a list of user angles.
4. Upwelling irradiance (total) at BOA. 

### FROM ROB

\#1 is the total upwelling radiance at BOA, obtained  for each geometrical index, g from `VLIDORT_Out%Main%TS_STOKES(2,g,1,1)`

\#2 and #3 are the direct upwelling water-leaving and glitter radiances respectively into the line of sight.

\#2 is obtained from `VLIDORT_Out%WLOut%TS_WLADJUSTED_DIRECT(1,v,s,a)` where v is the vza angle, a the azimuth and s the sun angle index.

`VLIDORT_Sup%SS%TS_STOKES_DB(2,g,1)` is actually equal to #2 + #3, so that #3 is `VLIDORT_Sup%SS%TS_STOKES_DB(2,g,1) - VLIDORT_Out%WLOut%TS_WLADJUSTED_DIRECT(1,v,s,a)`

The indices are related by `g =  N_VZAS * N_AZMS * ( s - 1 ) + N_AZMS * ( v - 1 ) + a`

I realize this is a little convoluted, so I think that I will introduce an output variable: `VLIDORT_Out%WLOut%TS_GLITTER_DIRECT(1,v,s,a)` so you can have the results directly.

**Items #2 and #3 are not completely independent of each other, since the VLIDORT code uses coupling of the water-leaving input.**

Also #1 = #2 + #3 + #5, where #5 is diffusely-reflected radiance

The flux #4 is `VLIDORT_Out%MAIN%TS_FLUX_DIFFUSE(2,s,1,1)`
