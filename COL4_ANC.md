```mermaid
graph TB
    subgraph "OMANC.f90"

        inputfile("Inputs (hdf5)")
        main[main.f90]
        input["input.f90"]
        combine["combine.f90"]
        output["output.f90"]
        met[metadata.f90]        
        outfile("OMUANC (netCDF)")
        fs(filespec)

        input --> main
        main --- combine
        main --- output

    subgraph Output processing
        met --> outfile
        output --> outfile
        output --- met
    end

    subgraph "Input processing"
        input
        fs --> input
        inputfile --> input

    end

    subgraph Aggregation
        combine
    end

    subgraph "people"
        ram[Ram]
        peter[Peter]
        zach[Zach/Dave]
    end

end

style met fill:lightgreen
style fs fill:lightgreen
style inputfile fill:orange
style fs fill:lightgreen
style inputfile fill:orange
style output fill:lightblue
style outfile fill:white
style input fill:orange
style combine fill:orange
style main fill:orange
style ram fill:lightblue
style peter fill:lightgreen
style zach fill:orange
```
